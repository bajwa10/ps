from django.urls import path
from . import views
from quiz.models import Bank


urlpatterns = [
    path('', views.manageQBank, name='manageQBank'),
    path('addquestion/', views.addform, name='addform'),
    path('addquestion/addform', views.add, name='add'),

    path('editquestions<str:qToEdit>/', views.edit, name='edit'),
    path('editquestions/editform<str:qToEdit>', views.editform, name='editform'),
    #path('editquestions/editform', views.edit, name='edit')
]
