from django.shortcuts import render, redirect
from quiz.models import Bank

# Create your views here.

def manageQBank(request):
    return render(request, 'manageQbank.html')

def edit(request,qToEdit='0'):
    if qToEdit == '0':
        allQuestions = Bank.objects.all()
        return render(request, 'editquestions.html', {'questions': allQuestions})
    else:
        qToUpdate=Bank.objects.get(id=qToEdit)
        qToUpdate.question = request.POST.get('question')
        qToUpdate.options = request.POST.get('options').split(',')
        qToUpdate.answer = request.POST.get('answer')
        qToUpdate.marks = request.POST.get('weightage')
        qToUpdate.level = request.POST.get('levels')
        qToUpdate.skill = request.POST.get('skills')
        qToUpdate.competency = request.POST.get('competencies')
        qToUpdate.topic = request.POST.get('topics')
        if request.POST.get('isActive')=='Yes':
            qToUpdate.isActive = True
        else:
            qToUpdate.isActive = False

        qToUpdate.save()
        allQuestions = Bank.objects.all()
        return redirect('/home')

    #return render(request, 'editquestions.html', {'questions': allQuestions})


def add(request):
    newQuestion = Bank()

    newQuestion.question = request.POST.get('question')
    newQuestion.options = request.POST.get('options').split(',')
    newQuestion.answer = request.POST.get('answer')
    newQuestion.marks = request.POST.get('weightage')
    newQuestion.level = request.POST.get('levels')
    newQuestion.skill = request.POST.get('skills')
    newQuestion.competency = request.POST.get('competencies')
    newQuestion.topic = request.POST.get('topics')
    newQuestion.save()

    return redirect('/home')

def addform(request):
    variables = {'skills': ['DevOps & Cloud'], 'competencies': ['DevOps Concepts', 'OS'],
                 'levels': ['SAL1', 'SAL2', 'AL2', 'MGR'], 'topics': ['AWS', 'Azure']}
    return render(request, 'addquestion.html', variables)

def editform(request,qToEdit):
    question = Bank.objects.get(id=qToEdit)

    #TEMPORARY : TO BE CHANGED LATER
    options_str=''
    for option in question.options:
        options_str = options_str + option +','


    variables = {'skills': ['DevOps & Cloud'], 'competencies': ['DevOps Concepts', 'OS'],
                 'levels': ['SAL1', 'SAL2', 'AL2', 'MGR'], 'topics': ['AWS', 'Azure'], 'qToEdit1': question,'options':options_str[:-1]}



    return render(request, 'editform.html', variables)




