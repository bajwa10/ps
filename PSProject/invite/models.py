from django.db import models
from jsonfield import JSONField
import datetime

# Create your models here.
class Test(models.Model):
    cUserId = models.TextField(default='NULL')
    aUserId = models.TextField(default='NULL')
    topic = models.TextField(default='NULL')
    date = models.DateField(default=datetime.date(2000,1,1))
    questions = JSONField(default = [])