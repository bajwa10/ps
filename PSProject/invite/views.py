from django.db.models import Q
from django.shortcuts import render, redirect
from login.models import Users
from invite.models import Test
from quiz.models import Bank
import datetime


# Create your views here.
def default(request):
    # variables will be retrieved from database at a later stage
    variables = {'skills': ['DevOps & Cloud'], 'competencies': ['DevOps Concepts', 'OS'],
                 'levels': ['SAL1', 'SAL2', 'AL2', 'MGR'], 'topics': ['AWS', 'Azure']}
    return render(request, 'invite.html', variables)


def invite(request):
    temp = {'name': request.POST['name'], 'email': request.POST['email'], 'level': request.POST['levels'],
            'skill': request.POST['skills'], 'competency': request.POST['competencies'],
            'topic': request.POST['topics'], 'date' : request.POST['date']}
    filter_results = Users.objects.filter(Q(userid=temp['email']) | ~Q(userid=temp['email']),
                                          firstname=temp['name'], email=temp['email'])
    # currently passwords for all are 123
    if len(filter_results) == 0:
        user = Users(userid=temp['email'], firstname=temp['name'], password=123, auth=1, email=temp['email'])
        user.save()
        testcreater(temp['email'], temp, request.session['username'])
        return redirect('/home')
    else:
        request.session['tempCandidate'] = temp
        return render(request, 'invite-results.html', {'users': filter_results})


def tests(request):
    tests = Test.objects.all()
    return render(request, 'viewTests.html', {'tests': tests})


# pk is the userid of an old candidate that the admin selected
def inviteold(request, pk):
    temp = request.session['tempCandidate']
    testcreater(pk, temp, request.session['username'])
    del request.session['tempCandidate']
    return redirect('/home')


def invitenew(request):
    temp = request.session['tempCandidate']
    user = Users(userid=temp['email'], firstname=temp['name'], password=123, auth=1, email=temp['email'])
    user.save()
    return redirect('/home')


def testcreater(userid, temp, admin):
    allQuestions = Bank.objects.all()
    testQuestions = []
    dateString = temp['date'].split('-')
    date  = list(map(int,dateString))
    for q in allQuestions:
        if q.topic == temp['topic'] and q.level == temp['level'] and q.skill == temp['skill'] and \
                q.competency == temp['competency']:
            print('success')
            testQuestions.append(q.id)
    test = Test(cUserId=userid, aUserId=admin, topic=temp['topic'],
                date=datetime.date(date[0], date[1],date[2]), questions=testQuestions)
    test.save()
