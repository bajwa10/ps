from django.urls import path
from . import views

urlpatterns = [
    path('', views.default, name='default'),
    path('invite', views.invite, name='default'),
    path('view_tests', views.tests, name='default'),
    path('inviteold/<str:pk>', views.inviteold, name='inviteOld'),
    path('invite-new', views.invitenew, name='inviteNew')
]
