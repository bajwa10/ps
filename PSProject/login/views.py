from django.shortcuts import render, redirect
from login.models import Users


def login(request):
    return render(request, 'index.html')


def home(request):
    try:
        userid = request.POST['user']
        password = request.POST['pass']
        if Users.objects.filter(userid=userid, password=password):
            request.session['username'] = userid
            user = Users.objects.get(userid=userid)
            request.session['auth'] = user.auth
            if user.auth == 1:
                return render(request, 'home.html', {'name': request.POST['user']})
            else:
                return render(request, 'adminhome.html', {'name': request.POST['user']})
        return redirect('/')
    except Exception:
        if request.session['auth'] == 1:
            return render(request, 'home.html', {'name': request.session['username']})
        else:
            return render(request, 'adminhome.html', {'name': request.session['username']})


def logout(request):
    request.session.flush()
    return redirect('/')
