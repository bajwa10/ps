from django.db import models


# Create your models here.

class Users(models.Model):
    userid = models.TextField(default='SOME STRING', max_length=10)
    password = models.TextField(default='SOME STRING')
    auth = models.IntegerField(default=1)
    firstname = models.TextField(default='SOME STRING')
    middlename = models.TextField(default='SOME STRING')
    lastname = models.TextField(default='SOME STRING')
    address = models.TextField(default='SOME STRING')
    email = models.TextField(default='SOME STRING')