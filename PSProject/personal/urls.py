from django.urls import path
from . import views

urlpatterns = [
    path('', views.personal, name='personalInfo'),
    path('personal', views.add, name='submit')
]
