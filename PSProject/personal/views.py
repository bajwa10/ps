from django.shortcuts import render, redirect
from login.models import Users


# Create your views here.
def personal(request):
    userid = request.session['username']
    user = Users.objects.get(userid=userid)
    return render(request, 'personal.html',
                  {'first': user.firstname, 'middle': user.middlename, 'last': user.lastname, 'email': user.email,
                   'address': user.address})


def add(request):
    userid = request.session['username']
    user = Users.objects.get(userid=userid)
    user.firstname = request.POST['first']
    user.middlename = request.POST['middle']
    user.lastname = request.POST['last']
    user.address = request.POST['address']
    user.email = request.POST['email']
    user.save()
    return redirect('/home')
