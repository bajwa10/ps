from django.urls import path
from . import views

urlpatterns = [
    path('', views.quiz, name='beginQuiz'),
    path('results', views.results, name='submit')
]