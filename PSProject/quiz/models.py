from jsonfield import JSONField
from django.db import models
from django import forms
from jsonfield import JSONField


# Create your models here.

class Bank(models.Model):
    question = models.TextField(default='NULL')
    type = models.BooleanField(default='True')  # True means MCQ Question and False means not
    skill = models.TextField(default='NULL')
    topic = models.TextField(default='NULL')
    competency = models.TextField(default='NULL')
    options = JSONField(default=[])
    level = models.TextField(default='AL2')
    answer = models.IntegerField(default=0)
    marks = models.IntegerField(default=0)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return str(self.question) + str(self.topic) + str (self.skill) +  str(self.level) + str(self.competency)
