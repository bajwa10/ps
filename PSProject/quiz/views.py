from os import name
from quiz.models import Bank
from django.shortcuts import render
from .models import Bank
from invite.models import Test


# Create your views here.
def quiz(request):
    test = Test.objects.get(cUserId = request.session['username'])
    qIds = test.questions
    allQuestions = Bank.objects.all()
    questions = []
    for q in allQuestions:
        if q.id in qIds:
            questions.append(q)


    return render(request, 'quiz.html', {"questions": questions})


def results(request):
    return render(request, 'results.html')
